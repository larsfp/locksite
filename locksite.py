#!/usr/bin/env python

import logging
import os
from optparse import OptionParser
import ConfigParser
import fnmatch
import pprint

system_version = '2.2'
system_name = 'locksite.py'


class ArgumentParser:
    def __init__(self):
        self.debug = False
        self.verbosity = False
        self.site_name = ""
        self.config_file = ""
        self.path = ""
        self.action = "lock"

    def parse_input_arguments(self):

        description = "%s %s locks the directories and files of the site using chattr" \
            % (system_name, system_version)
        usage = "usage: %prog [options] sitename"
        parser = OptionParser(usage=usage, description=description)
        parser.add_option("-o", "--config_file", help="path to the config file", dest="config_file")
        parser.add_option("-d", "--debug", help="debug mode", action='store_true', dest="debug")
        parser.add_option("-v", "--verbose", help="verbosity mode", action='store_true', dest="verbose")
        parser.add_option("-p", "--path", help="single path to lock/unlock", action='store', dest="path")
        parser.add_option("-u", "--unlock",
                          help="unlock site instead of default lock site",
                          action='store_const', const='unlock',
                          dest="action")

        options, args = parser.parse_args()

        # print ("Arguments given:")
        # print (options)

        if len(args) != 1 and not options.config_file:
            parser.error("You must enter a site name")
        if options.action:  # Only update value if something is given.
            self.action = options.action
        if options.verbose and options.debug:
            parser.error("options -d and -v are mutually exclusive")
        if not options.config_file:
            self.site_name = args[0]
        self.config_file = options.config_file
        self.verbosity = options.verbose
        self.debug = options.debug
        if self.debug:
            logging.basicConfig(level=logging.DEBUG)
        if self.verbosity:
            logging.basicConfig(level=logging.INFO)
        self.path = options.path
        if self.path and not args:
            exit("Error: -p / --path given, but no path supplied. See help")

    @staticmethod
    def check_user():
        if os.geteuid() != 0:
            exit("You need root privileges, please use 'sudo'.")
            # print("You need root privileges, please use 'sudo'.")  # for debugging


class Command:
    def __init__(self):
        self.input_parser = ArgumentParser()
        self.config = ConfigParser.RawConfigParser(allow_no_value=True)
        self.web_dir = ""
        self.included_dirs = []
        self.excluded_dirs = []
        self.full_tree = []
        self.processed_files = 0

    def parse_input_arguments(self):
        self.input_parser.parse_input_arguments()
        self.input_parser.check_user()

    @staticmethod
    def _exit_with(message, exit_code):
        import sys

        print ('***', message, '***')
        print ('Exit with code:', exit_code)
        sys.exit(exit_code)

    def _call_external_command(self, full_command):
        import subprocess

        p = None
        try:
            p = subprocess.Popen(full_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except OSError, ex:
            self._exit_with('*** Command: %s isn\'t available *** %s' % (full_command, ex.strerror), 2)
        output, ret = p.communicate()
        logging.info(output)
        return output, p.returncode

    def read_config_file(self):

        if not self.input_parser.config_file:
            if ".conf" in self.input_parser.site_name:
                self.input_parser.config_file = self.input_parser.site_name
            else:
                self.input_parser.config_file = self.input_parser.site_name + ".conf"

        if not os.path.exists(self.input_parser.config_file):
            self._exit_with("Unable to open config file %s" % self.input_parser.config_file, 1)
        self.config.read(self.input_parser.config_file)

    def parse_config_file(self):
        # Print config to debug log
        for section_name in self.config.sections():
            logging.debug('Section:' + section_name)
            for name, value in self.config.items(section_name):
                if value:
                    logging.debug('  %s = %s' % (name, value))
                else:
                    logging.debug('  %s' % name)

        try:
            self.web_dir = os.path.abspath(self.config.get(section='site', option='webdir'))
            logging.debug("webdir set to %s" % self.web_dir)

            for directory, placeholder in self.config.items('directories'):
                if directory.startswith('-'):
                    self.excluded_dirs.append(os.path.abspath(self.web_dir + directory.split('-', 1)[1]))
                elif directory.startswith('+'):
                    self.included_dirs.append(os.path.abspath(self.web_dir + directory.split('+', 1)[1]))
                else:
                    self.included_dirs.append(os.path.abspath(self.web_dir + directory))

            logging.debug("Included dirs %s" % self.included_dirs)
            logging.debug("Excluded dirs %s" % self.excluded_dirs)
        except ConfigParser.NoSectionError, message:
            self._exit_with("Configuration error in %s: %s" % (self.input_parser.config_file, message), 2)

        if not os.path.exists(self.web_dir):
            self._exit_with("Error, webdir %s doesn't exist." % self.web_dir, 1)

    def list_recursive_directories(self):
        print "Processing %s" % self.web_dir
        paths = []
        # character_limit = 130000  # default limit in the typical system is 131062

        for root, directories, files in os.walk(self.web_dir, topdown=False, followlinks=False):
            for name in (files + directories):
                absolute = os.path.join(root, name)

                if os.path.islink(absolute): # Skip symlinks
                    continue

                if not self.check_exclude(absolute) or self.check_include(absolute):
                    paths.append(absolute)

        if not self.check_exclude(self.web_dir):
            paths.append(self.web_dir)

        # print("Locking these files/directories:")
        # pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(paths)

        self.full_tree = paths

    def check_include(self, filename):
        return any(fnmatch.fnmatch(filename, pattern) for pattern in self.included_dirs)

    def check_exclude(self, filename):
        return any(fnmatch.fnmatch(filename, pattern) for pattern in self.excluded_dirs)

    def apply_lock(self):
        character_limit = 130000  # default limit in the typical system is 131062
        path_count = len(self.full_tree)

        action = '+'
        if 'unlock' == self.input_parser.action:
            action = '-'

        print("Number of files/dirs to process: %s " % path_count)
        
        # Batch processing paths
        arguments = []
        for id, path in enumerate(self.full_tree):
            arguments.append(path)

            # Check that length of all strings doesn't overflow cli
            if character_limit <= sum(map(len, arguments)) or id >= (path_count-1):
                cmd = ["chattr", action + "i"] + arguments
                logging.debug("Cmd %s" % cmd)

                print("Applying to %s files ..." % len(arguments))

                # print("Locking these files/directories:")
                # pp = pprint.PrettyPrinter(indent=4)
                # pp.pprint(cmd)

                output, return_code = self._call_external_command(cmd)
                if return_code:
                    self._exit_with(output, return_code)
                self.processed_files += len(arguments)

                arguments = []

    def print_summary(self):
        print ("The site has been successfully %sed." % self.input_parser.action)
        print ("Number of %sed dirs/files: %s"
               % (self.input_parser.action, self.processed_files))


if __name__ == "__main__":
    command = Command()
    command.parse_input_arguments()
    command.read_config_file()
    command.parse_config_file()
    command.list_recursive_directories()
    command.apply_lock()
    command.print_summary()
