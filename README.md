Locksite 2:
===========

Home
--

* https://gitlab.com/larsfp/locksite


License
--

GPL


Developers
--

* Fernando Berretti <fernandoberretti@gmail.com>
* Lars Falk-Petersen <lars@cl.no>

Example
--

# ./locksite.py example.no.conf
Processing /home/example/www
Number of files/dirs to process: 8146
Applying to 2111 files ...
Applying to 1477 files ...
Applying to 1303 files ...
Applying to 1495 files ...
Applying to 1760 files ...
The site has been successfully locked.
Number of locked dirs/files: 8146

# touch /home/example/www/wp-mail.php
touch: cannot touch '/home/example/www/wp-mail.php': Permission denied


Release log
--

2.1 - batch processing for speed.
2.0 - better config, exclude and include files/directories. Merge to one file instead of lockdown / unlock.
1.0 - First version.
